using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class StaticObject : MonoBehaviour, IHitBox
{
    [SerializeField] private LevelObjectData objectData;
    private int health = 1;
    private Rigidbody2D rigidbody;

    private void Start()
    {
        health = objectData.Health;
        rigidbody = GetComponent<Rigidbody2D>();
        rigidbody.bodyType = objectData.IsStatic ? RigidbodyType2D.Static : RigidbodyType2D.Dynamic;
    }

#if UNITY_EDITOR
    [ContextMenu("Rename this")]
    private void Rename()
    {
        if (objectData != null)
        {
            gameObject.name = objectData.Name;
        }
    }

    [ContextMenu("Move Right")]
    private void MoveRight()
    {
        Move(Vector2.right);
    }

    [ContextMenu("Move Left")]
    private void MoveLeft()
    {
        Move(Vector2.left);
    }
    
    [ContextMenu("Move Copy Up")]
    private void CopyAndMoveUp()
    {
        Instantiate(gameObject);
        Move(Vector2.up);
    }

    private void Move(Vector2 direction)
    {
        var collider = GetComponent<Collider2D>();
        var size = collider.bounds.size;
        
        transform.Translate(direction * size);
    }
    
#endif

    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0f)
            {
                Die();
            }
        }
    }
    public void Hit(int damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        print("Object is died");
    }
}