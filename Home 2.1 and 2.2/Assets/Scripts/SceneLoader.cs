using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class SceneLoader : MonoBehaviour
{
      private static string nextLevel;

      private SpriteRenderer rend;
      
      [SerializeField] private GameObject LoadingIce;
      [SerializeField] private GameObject[] sceneObjects;

      public static void LoadLevel(string level)
      {
            nextLevel = level;
            SceneManager.LoadScene("LoadingScene");
      }

      IEnumerator FadeIn()
      {
          for (float f = 0.05f; f <=1; f+=0.05f)
          {
              rend = sceneObjects[0].GetComponent<SpriteRenderer>();
              Color c = rend.material.color;
              c.a = f;
              rend.material.color = c;
              yield return new WaitForSeconds(0.05f);
          }
      }
      IEnumerator FadeOut()
      {
          for (float f = 1f; f >=-0.05; f-=0.05f)
          {
              rend = sceneObjects[0].GetComponent<SpriteRenderer>();
              Color c = rend.material.color;
              c.a = f;
              rend.material.color = c;
              yield return new WaitForSeconds(0.05f);
          }
      }
      
      private IEnumerator Start()
      {
          sceneObjects[1].SetActive(false);
          sceneObjects[2].SetActive(false);
          StartCoroutine("FadeIn");
          sceneObjects[1].SetActive(true);
          sceneObjects[2].SetActive(true);


          GameManager.SetGameState(GameState.Loading);

           for (int j = 0; j < 100; j++)
           {
               yield return new WaitForSeconds(0.05f);
               LoadingIce.transform.localScale += new Vector3(+0.01f, 0, 0);;
           }
           StartCoroutine("FadeOut");
           sceneObjects[1].SetActive(false);
           sceneObjects[2].SetActive(false);
           
           yield return new WaitForSeconds(1f);
               

               if (string.IsNullOrEmpty(nextLevel))
           {
               SceneManager.LoadScene("MainMenuScene");
               yield break;
           }
          
           AsyncOperation loading = null;
           
           
           loading = SceneManager.LoadSceneAsync(nextLevel, LoadSceneMode.Additive);
           
           
           while (!loading.isDone)
           {
               yield return null;
           }

           nextLevel = null;
           
           
           
           SceneManager.UnloadSceneAsync("LoadingScene");
           
      }
}