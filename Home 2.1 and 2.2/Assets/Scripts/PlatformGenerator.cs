﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

public class PlatformGenerator : MonoBehaviour
{
    [SerializeField] private Sprite startPart1;
    [SerializeField] private Sprite endPart1;
    [SerializeField] private Sprite betweenPart1;
    [SerializeField] private Sprite startPart2;
    [SerializeField] private Sprite endPart2;
    [SerializeField] private Sprite betweenPart2;

    private int randomStartPart;
    private int randomEndPart;
    private int randomBetweenPart;
    private int randomDistanceBetweenPlatforms;

    private GameObject[] allPlatforms;
    
    private float randomY;
    private double X = 8;

    public int[] sizesOfPlatforms;

    public bool generateLevel=false;

    private void Update()
    {
        if (generateLevel)
        {
            GenerateLevel();
            generateLevel = false;
        }
  
    }
    
    void GenerateLevel()
    {
        for (int i = 0; i < sizesOfPlatforms.Length; i++)
        {
            randomDistanceBetweenPlatforms = Random.Range(1, 4);
            X += randomDistanceBetweenPlatforms;
            randomY = Random.Range(-3, 1);
            
            for (int j = 0; j < sizesOfPlatforms[i]; j++)
            {
                GameObject obj = new GameObject($"Generated platform {i}.{j}");
                SpriteRenderer spriteRenderer = obj.AddComponent<SpriteRenderer>();

                if (j == 0)//передняя часть платформы
                {
                    randomStartPart = Random.Range(1, 3);
                    spriteRenderer.sprite = randomStartPart==1 ? startPart1 : startPart2;
                }

                if ((j != 0) && (j != sizesOfPlatforms[i] -1))//промежуточные части платформы
                {
                    randomBetweenPart = Random.Range(1, 3);
                    spriteRenderer.sprite = randomStartPart==1 ? betweenPart1 : betweenPart2;
                }
                
                if (j == sizesOfPlatforms[i] -1)//задняя часть платформы
                {
                    randomEndPart = Random.Range(1, 3);
                    spriteRenderer.sprite = randomStartPart==1 ? endPart1 : endPart2;
                }
                
                spriteRenderer.sortingLayerName = "Background";
                spriteRenderer.sortingOrder = 2;
                
                BoxCollider2D boxCollider2D = obj.AddComponent<BoxCollider2D>();
                boxCollider2D.size = new Vector2(1.28f,1.28f);
                
                obj.transform.position = new Vector3((float)X, randomY, 0);
                X += 1.28;
                
            }
            
        }
    }
}

