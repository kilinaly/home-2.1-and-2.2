﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Platforms", menuName = "Platforms")] 

public class PlatformScriptableObject : ScriptableObject
{

    [SerializeField] public Sprite startPart1;
    [SerializeField] private Sprite endPart1;
    [SerializeField] private Sprite betweenPart1;
    [SerializeField] private Sprite startPart2;
    [SerializeField] private Sprite endPart2;
    [SerializeField] private Sprite betweenPart2;


    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
